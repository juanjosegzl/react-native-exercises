import React, { Component } from "react";
import { Platform, StyleSheet, View, FlatList, Button } from "react-native";
import Question from "./Question";
import AppBar from "./AppBar";
import FAB from "./FAB";
import NewQuestionForm from "./NewQuestionForm";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Response from "../mockedResponse";
import Details from "./Details";

const questions = Response.questions;

class App extends Component {
  static navigationOptions = {
    title: 'Questions',
  };

    render() {
        return (
            <View style={{backgroundColor: "white"}}>
            <FlatList
            style={{padding: 16}}
            renderItem={({item}) => (<Question
                author={item.created_by_user ? item.created_by_user.full_name : ''}
                assigned={item.assigned_to_user ? item.assigned_to_user.full_name : ''}
                department={item.department}
                question={item.question}
                                     answer={item.Answer.answer}
                                     question_id={item.question_id}
                created={item.createdAt}
                                     numberOfLikes={item.Votes}
                                     numberOfComments={item.numComments}
                                     onComment={() => this.props.navigation.navigate('Details', {item, target:'Comments'})}
                                     onExpand={() => this.props.navigation.navigate('Details', {item})}

                />)
            }
            data={questions}
            />

            <View>
            <FAB onPress={() => this.props.navigation.navigate('New')}/>
            </View>
            </View>
        );
    }
}

const AppNavigator =  createStackNavigator(
{
  Home: App,
  New: NewQuestionForm,
  Details: Details,
},
{
    initialRouteName: 'Home',
    defaultNavigationOptions: {
        title: "Questions",
        headerStyle: {
            backgroundColor: '#990100',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
      headerRight: Platform.OS === "ios" && (
          <Button
        title="Add"
        color="#fff"
          />
    ),
    }
}
);

export default createAppContainer(AppNavigator);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 16
    },
});
