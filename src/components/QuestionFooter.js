import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

export default class QuestionFooter extends Component {
  render() {
    return (
        <View style={styles.footerContainer}>
        <TouchableOpacity onPress={() => this.props.onLike()}>
        <Text>{this.props.likes}</Text>
        </TouchableOpacity>
        <Text>{this.props.comments}</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 12,
    flexDirection: "row"
  },
});
