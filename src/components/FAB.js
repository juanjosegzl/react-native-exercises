import React, { Component } from "react";
import { Platform, View, Text, StyleSheet } from "react-native";

import { TouchableNativeFeedback, TouchableOpacity } from "react-native";

const Touchable = Platform.select({
  ios: TouchableOpacity,
  android: TouchableNativeFeedback
});

export default class FAB extends Component {
  render () {
    return (
        <View style={ styles.fab }>
        <Touchable onPress={this.props.onPress} background={TouchableNativeFeedback.Ripple('black', true)}>
        <View style={styles.button}>
        <Text style={styles.title}>+</Text>
        </View>
        </Touchable>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  fab: {
    borderRadius: 50,
    position: "absolute",
    bottom: 0,
    right: 0,
    margin: 16,
    elevation: 8,
    height: 56,
    width: 56,
  },
  button: {
    flex: 1,
    height: 56,
    width: 56,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#990100",
  },
  title: {
    fontSize: 24,
    color: "white",
  }
});
