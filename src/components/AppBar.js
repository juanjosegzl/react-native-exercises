import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

export default class AppBar extends Component {
  render () {
    return (
        <View style={styles.bar}>
        <Text style={styles.title}>{ this.props.title }</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  bar: {
    backgroundColor: "#990100",
    height: 56,
    padding: 16,
    elevation: 8,
    position: "absolute",
    top: 0,
  },
  title: {
    fontSize: 20,
    color: "white",
  }
});
