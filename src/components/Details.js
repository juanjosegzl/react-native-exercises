import React, { Component } from "react";
import { View, Text, StyleSheet, Platform } from "react-native";
import Card from "./Card";
import moment from "moment";
import { createBottomTabNavigator, createMaterialTopTabNavigator, createAppContainer } from "react-navigation";
import Comments from "./Comments";

const createTabNavigator = Platform.select({
  ios: createBottomTabNavigator,
  android: createMaterialTopTabNavigator
});

class Details extends Component {
  render() {
    const { navigation } = this.props;
    const question = navigation.getParam('item');
    if (navigation.getParam('target') === 'Comments') {
      this.props.navigation.navigate('Comments');
    }


    return (
        <View>
        <Card>
        <Text>From {question.created_by_user ? question.created_by_user.full_name : ''} To {question.assigned_to_user ? question.assigned_to_user.full_name : ''}</Text>
          <Text>Created { moment(question.createdAt).fromNow() }</Text>
        <Text style={styles.questionText}>
        {question.question}
      </Text>
        <Text>
        {question.Answer.answer || "No answered yet"}
      </Text>
        </Card>
        </View>
    );
  }
}

const TabNavigator = createTabNavigator(
  {
    Details: Details,
    Comments: Comments,
  },
  {
    tabBarOptions: {
      activeTintColor: '#990100',
      inactiveTintColor: 'gray',
      style: { backgroundColor: '#fff'},
      indicatorStyle: { backgroundColor: '#990100' }
    }
  });

export default createAppContainer(TabNavigator);


const styles = StyleSheet.create({
  questionText: {
    color: "#222",
    textAlign: "justify",
    fontWeight: "bold",
    marginTop: 16,
    marginBottom: 16
  },
  actions: {
    flexDirection: "row",
    marginTop: 16,
  },
  iconActions: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
});
