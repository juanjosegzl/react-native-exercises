import React, { Component } from "react";
import { View, TextInput, Text } from "react-native";

export default class NewQuestionsForm extends Component {
  static navigationOptions = {
    title: 'Ask a question',
  };

  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  render() {
    return (
        <View>
        <TextInput
        style={{height: 40}}
        placeholder="Type title"
        onChangeText={(text) => this.setState({text})}
        />
        <Text style={{padding: 10, fontSize: 42}}>
        {this.state.text.toUpperCase()}
        </Text>
        </View>
    );
  }
};
