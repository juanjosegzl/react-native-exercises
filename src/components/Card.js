import React, { Component } from "react";
import { Platform, View, StyleSheet } from "react-native";

import { TouchableNativeFeedback, TouchableOpacity } from "react-native";

let Touchable = TouchableNativeFeedback;
if (Platform.OS !== "android") {
  Touchable = TouchableOpacity;
}

export default class Card extends Component {
  render() {
    return (
        <Touchable onPress={this.props.onPress}>
        <View style={styles.card}>
            { this.props.children }
      </View>
        </Touchable>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    elevation: 1,
    justifyContent: 'space-between',
    backgroundColor: "white",
    borderRadius: 12,
    padding: 16,
    marginBottom: 12
  }
});
