import React, { Component } from "react";
import { Platform, View, Text, StyleSheet } from "react-native";
import QuestionFooter from "./QuestionFooter";
import Card from "./Card";
import moment from "moment";
import Icon from 'react-native-vector-icons/FontAwesome';

import { TouchableNativeFeedback, TouchableOpacity } from "react-native";

let Touchable = TouchableNativeFeedback;
if (Platform.OS !== "android") {
  Touchable = TouchableOpacity;
}

export default class Question extends Component {
  constructor(props) {
    super(props);
    this.state = {
      likes: this.props.numberOfLikes,
      numberOfLinesQuestion: 3,
      numberOfLinesAnswer: 5,
      expanded: false,
      liked: false,
    };
  }

  handleLike = () => {
    if (! this.state.liked) {
      this.setState(previous => (
        {
          likes: previous.likes + 1,
          liked: !previous.liked
        }
      ));
    } else {
      this.setState(previous => (
        {
          likes: previous.likes - 1,
          liked: !previous.liked
        }
      ));
    }
  }

  toggle = () => {
    if (this.state.expanded) {
      this.collapse();
    } else {
      this.expand();
    }
  }

  expand = () => {
    this.setState(() => ({
      numberOfLinesQuestion: 0,
      numberOfLinesAnswer: 0,
      expanded: true
    }));
  }

  collapse = () => {
    this.setState(() => ({
      numberOfLinesQuestion: 3,
      numberOfLinesAnswer: 5,
      expanded: false
    }));
  }

  render() {
    return (
        <Card onPress={this.toggle}>
  <Text>From {this.props.author} To {this.props.assigned || this.props.department}</Text>
  { this.state.expanded &&
  <Text>Created { moment(this.props.created).fromNow() }</Text>
  }
  <Text
    numberOfLines={ this.state.numberOfLinesQuestion}
    style={styles.questionText}
    >
    {this.props.question}
  </Text>
  <Text
    numberOfLines={ this.state.numberOfLinesAnswer}
    >
    {this.props.answer || "No answered yet"}
  </Text>
  <View style={styles.actions}>

        <Touchable onPress={this.props.onExpand}>
      <Text>Details</Text>
    </Touchable>
    <View style={styles.iconActions}>

        <Touchable onPress={this.handleLike}>
        <View style={{flexDirection: "row", marginRight: 16}}>
        <Icon name="heart" size={16} color={this.state.liked ? 'red' : 'gray' } />
        <Text style={{marginLeft: 8}}>{this.state.likes}</Text>
        </View>
      </Touchable>

      <Touchable onPress={this.props.onComment}>
        <View style={{flexDirection: "row", marginRight: 16}}>
        <Icon name="comment" size={16} color='gray'/>
        <Text style={{marginLeft: 8}}>{this.props.numberOfComments}</Text>
        </View>
      </Touchable>

    </View>
  </View>
</Card>
    );
  }
}

const styles = StyleSheet.create({
  questionText: {
    color: "#222",
    textAlign: "justify",
    fontWeight: "bold",
    marginTop: 16,
    marginBottom: 16
  },
  actions: {
    flexDirection: "row",
    marginTop: 16,
  },
  iconActions: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
});
